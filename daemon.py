#! /usr/bin/env python

import sys
from subprocess import Popen, PIPE, call
import time
from twinkle_util import tw_cmd
import ui

LOG_INFO = lambda s: print("["+str(time.strftime("%y%m%d-%H%M%S"))+"] "+str(s))

# Open Twinkle console for output parsing
twinkle = Popen(["twinkle", "-c"], stdout=PIPE)

# Get iterator of twinkle console and loop through it
for line in iter(twinkle.stdout.readline, b""):

    line = str(line)

    # Handle different events based on Twinkle output
    # Registration successful
    if line.find("registration succeeded") > 0:
        LOG_INFO("Client registered with Asterisk")

    # Incoming call
    elif line.find("incoming call") > 0:
        LOG_INFO("Incoming call")
        ui.answer_ui()

    # Call answered
    elif line.find("call established") > 0 or line.find("far end answered call") > 0:
        LOG_INFO("Call connected")
        ui.call_ui()

twinkle.communicate()
