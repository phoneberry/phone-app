from subprocess import call

def tw_cmd(command):
    command_full  = ["twinkle", "--cmd"] + [command]
    call(command_full)
