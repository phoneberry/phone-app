#! /usr/bin/env python

import tkinter
from twinkle_util import tw_cmd
import time


def reject_cb(root):
    print("REJECT")
    tw_cmd("reject")
    root.destroy()

def accept_cb(root):
    print("ACCEPT")
    tw_cmd("answer")
    root.destroy()

def hangup_cb(root):
    print("HANGUP")
    tw_cmd("bye")
    root.destroy()

def dial_cb(num, root):
    print("DIAL " + str(num))
    tw_cmd("call " + str(num))

def answer_ui():

    root = tkinter.Tk()
    root.attributes("-type", "dialog")

    reject = tkinter.Button(root, text="Reject", command=lambda: reject_cb(root))
    accept = tkinter.Button(root, text="Accept", command=lambda: accept_cb(root))

    reject.pack()
    accept.pack()

    root.mainloop()

then=0
def call_ui():
    global then
    then = time.time()

    root = tkinter.Tk()
    root.attributes("-type", "dialog")

    hangup = tkinter.Button(root, text="Hang Up", command=lambda: hangup_cb(root))
    clock = tkinter.Label(root, text="")

    clock.pack()
    hangup.pack()

    def clock_update():
        global then
        now = time.time()
        clock.configure(text=(f"%02d:%02d") % divmod(round(now-then), 60))
        root.after(1000, clock_update)

    clock_update()
    root.mainloop()

def dial_ui():

    root = tkinter.Tk()
    root.attributes("-type", "dialog")

    number = tkinter.Entry()
    dial = tkinter.Button(text="Dial", command=lambda: dial_cb(number.get(), root))

    number.pack()
    dial.pack()

    root.mainloop()

if __name__ == "__main__":
    dial_ui()
